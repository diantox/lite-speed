FROM python:3.9-buster

# Install pgcli.
RUN apt update
RUN apt install -y build-essential libpq-dev
RUN pip install pgcli

# Run pgcli.
CMD pgcli postgres://"$POSTGRES_USER":"$POSTGRES_PASSWORD"@"$POSTGRES_HOST"/"$POSTGRES_DB"
