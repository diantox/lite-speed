#!/bin/bash

# Constants --------------------------------------------------------------------
DOCKER_FILE=`realpath ./dockerfiles/pgcli.Dockerfile`
DOCKER_IMAGE_NAME='lite-speed-pgcli'
DOCKER_IMAGE_TAG='0.0.0'

# Utility Functions ------------------------------------------------------------

# Usage: print message
print() {
  printf "$1\n"
}

# Usage: warning warning_message
warning() {
  printf "\e[33m$1\e[m\n"
}

# Usage: error error_message
error() {
  printf "\e[31m$1\e[m\n"
}

# Main Functions ---------------------------------------------------------------

help() {
  print "Usage: `basename $0` [options]"
  print "  -h Display help."
  print "  -b Build a $DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG image."
  print "  -r Run a $DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG image."
}

build() {
  local DOCKER_RESPONSE=`docker image inspect "$DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG" 2> /dev/null`
  local DOCKER_ERROR_CODE=$?
  if [[ "$DOCKER_ERROR_CODE" == "0" ]]; then
    warning "Found an existing $DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG image..."
    warning "Delete image (y/n)?"

    read DELETE_DOCKER_IMAGE
    if [[ "$DELETE_DOCKER_IMAGE" == "y" || "$DELETE_DOCKER_IMAGE" == "Y" ]]; then
      docker rmi "$DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG"
    fi
  fi

  print "Building a new $DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG image..."
  docker build --file="$DOCKER_FILE" --tag="$DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG" `realpath ./.dockercontext`
}

run() {
  docker run \
    --env-file=`realpath ./envs/postgres.env` \
    --tty \
    --interactive \
    --rm \
    "$DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG"
}

while getopts 'hbsra' opt; do
  case "$opt" in
    h)
      help
      ;;
    b)
      build
      ;;
    r)
      run
      ;;
    *)
      help
      exit 128
      ;;
  esac
done

if [[ -z "$1" ]]; then
  help
  exit 128
fi
