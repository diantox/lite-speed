const AutoBind = require('auto-bind')
const Debug = require('debug')
const toField = require('../utils/to-field')
const getRequestedAs = require('../utils/get-requested-as')
const getRequestedFields = require('../utils/get-requested-fields')
const getRequestedSubresolvers = require('../utils/get-requested-subresolvers')

/**
 * Object representing a [Resolver]{@link Resolver}'s field (or subresolver if primitive is false).
 * @typedef {Object} ResolverField
 * @property {string} type - The field's type.
 * @property {boolean} list - Whether the field is a list.
 * @property {boolean} nonNull - Whether the field is non-nullabe.
 * @property {boolean} primitive - Whether the field's type is a primitive.
 * @property {string} name - The field's name.
 */

/**
 * Object representing a [Resolver]{@link Resolver}'s context (unique to each operation and Resolver).
 * @typedef {Object} ResolverContext
 * @property {ResolverField} requestedAs - The subresolver a Resolver was requested as.
 * @property {ResolverField[]} requestedFields - The fields requested by a Resolver.
 * @property {ResolverField[]} requestedSubresolvers - The subresolvers requested by a Resolver.
 *
 * @property {*} prefetch - The data returned by a Resolver's [prefetch]{@link Resolver#prefetch} phase.
 * @property {*} fetch - The data returned by a Resolver's [fetch]{@link Resolver#fetch} phase.
 *
 * @property {*} precreate - The data returned by a Resolver's [precreate]{@link Resolver#precreate} phase.
 * @property {*} create - The data returned by a Resolver's [create]{@link Resolver#create} phase.
 *
 * @property {*} preupdate - The data returned by a Resolver's [preupdate]{@link Resolver#preupdate} phase.
 * @property {*} update - The data returned by a Resolver's [update]{@link Resolver#update} phase.
 *
 * @property {*} predelete - The data returned by a Resolver's [predelete]{@link Resolver#predelete} phase.
 * @property {*} delete - The data returned by a Resolver's [delete]{@link Resolver#delete} phase.
 */

/**
 * Function representing a [Resolver]{@link Resolver}'s relation to another Resolver.
 * @callback ResolverRelationHandler
 * @param {*} parent - The data returned by a Resolver's parent.
 * @param {*} args - The arguments a Resolver is called with.
 * @param {*} context - GraphQL context.
 * @param {*} info - GraphQL info.
 */

/**
 * Class representing a GraphQL resolver.
 * @property {Object.<string, ResolverRelationHandler>} parentHandlers - A Resolver's parent handlers.
 * @property {Object.<string, ResolverRelationHandler>} childrenHandlers - A Resolver's children handlers.
 * @property {Object.<string, ResolverRelationHandler>} argumentHandlers - A Resolver's argument handlers.
 * @property {string} typeName - A Resolver's type name.
 * @property {string} fieldName - A Resolver's field name.
 * @property {ResolverField[]} fields - A Resolver's fields.
 * @property {Function} debug - A Resolver's [Debug]{@link https://github.com/visionmedia/debug} instance.
 */
class Resolver {

  static parentHandlers
  static childrenHandlers
  static argumentHandlers
  
  typeName
  fieldName
  fields
  debug

  /**
   * Create a new Resolver.
   * @param {*} schema - The GraphQL schema.
   */
  constructor(schema) {
    this.typeName = this.constructor.name
    this.fieldName = this.typeName[0].toLowerCase() + this.typeName.substring(1)
    this.fields = Object.values(schema._typeMap[this.typeName]._fields)
      .reduce(
        (fields, field) => {
          fields[field.name] = toField(field.astNode)
          return fields
        },
        {}
      )

    this.debug = Debug(this.typeName)

    const unhandledParents = Object.values(schema._typeMap)
      .filter(type => {
        if (
          type.name !== schema._queryType.name &&
          type.name !== schema._mutationType.name &&
          !type.name.endsWith('Operations') &&
          type.astNode &&
          type.astNode.kind == 'ObjectTypeDefinition' &&
          type._fields
        ) {
          const field = Object.values(type._fields)
            .find(field => {
              const resolverField = toField(field.astNode)
              return resolverField.type === this.typeName &&
                !resolverField.primitive
            })

          return !!field
        }

        return false
      })
      .filter(type =>
        !this.constructor.parentHandlers ||
        !this.constructor.parentHandlers[type.name])
      .map(type => type.name)


    if (unhandledParents.length > 0) {
      throw new Error(`${this.typeName} has unhandled parents; ${unhandledParents.join(', ')}.`)
    }

    const unhandledChildren = Object.values(this.fields)
      .filter(field => !field.primitive)
      .filter(field =>
        !this.constructor.childrenHandlers ||
        !this.constructor.childrenHandlers[field.name])
      .map(field => field.name)

    if (unhandledChildren.length > 0) {
      this.debug(`${this.typeName} has unhandled children; ${unhandledChildren.join(', ')}.`)
    }

    AutoBind(this)
    Object.freeze(this)
  }

  /**
   * @private
   *
   * @param {*} parent - The data returned by a Resolver's parent.
   * @param {*} args - The arguments a Resolver is called with.
   * @param {*} context - GraphQL context.
   * @param {*} info - GraphQL info.
   * @returns {*} The data returned by a Resolver's [postfetch]{@link Resolver#postfetch} phase.
   */
  async _fetch(parent, args, context, info) {
    await this.authorize('fetch', context)

    const requestedAs = getRequestedAs(this.typeName, info)
    const requestedFields = getRequestedFields(this.fields, info)
    const requestedSubresolvers = getRequestedSubresolvers(this.fields, info)

    const prefetch = await this.prefetch(
      parent,
      args,
      context,
      info,

      { requestedAs,
        requestedFields,
        requestedSubresolvers }
    )

    if (
      this.constructor.parentHandlers &&
      this.constructor.parentHandlers[info.parentType.name]
    ) {
      await this.constructor.parentHandlers[info.parentType.name](
        parent,
        args,
        context,
        info,

        { requestedAs,
          requestedFields,
          requestedSubresolvers,
          prefetch }
      )
    }

    if (this.constructor.childrenHandlers) {
      const promises = requestedSubresolvers
        .map(subresolver => {
          if (this.constructor.childrenHandlers[subresolver.type]) {
            return this.constructor.childrenHandlers[subresolver.type](
              parent,
              args,
              context,
              info,

              { requestedAs,
                requestedFields,
                requestedSubresolvers,
                prefetch }
            )
          }

          return null
        })
        .filter(promise => !!promise)

      await Promise.all(promises)
    }

    if (this.constructor.argumentHandlers) {
      const promises = Object.keys(args)
        .map(arg => {
          if (this.constructor.argumentHandlers[arg]) {
            return this.constructor.argumentHandlers[arg](
              parent,
              args,
              context,
              info,

              { requestedAs,
                requestedFields,
                requestedSubresolvers,
                prefetch }
            )
          }

          return null
        })
        .filter(promise => !!promise)

      await Promise.all(promises)
    }

    const fetch = await this.fetch(
      parent,
      args,
      context,
      info,

      { requestedAs,
        requestedFields,
        requestedSubresolvers,
        prefetch }
    )

    const postfetch = await this.postfetch(
      parent,
      args,
      context,
      info,

      { requestedAs,
        requestedFields,
        requestedSubresolvers,
        prefetch,
        fetch }
    )

    return postfetch
  }

  /**
   * @private
   *
   * @param {*} args - The arguments a Resolver is called with.
   * @param {*} context - GraphQL context.
   * @param {*} info - GraphQL info.
   * @returns {*} The data returned by a Resolver's [postcreate]{@link Resolver#postcreate} phase.
   */
  async _create(args, context, info) {
    await this.authorize('create', context)

    const requestedAs = getRequestedAs(this.typeName, info)
    const requestedFields = getRequestedFields(this.fields, info)
    const requestedSubresolvers = getRequestedSubresolvers(this.fields, info)

    const precreate = await this.precreate(
      args,
      context,
      info,

      { requestedAs,
        requestedFields,
        requestedSubresolvers }
    )

    const create = await this.create(
      args,
      context,
      info,

      { requestedAs,
        requestedFields,
        requestedSubresolvers,
        precreate }
    )

    const postcreate = await this.postcreate(
      args,
      context,
      info,

      { requestedAs,
        requestedFields,
        requestedSubresolvers,
        precreate,
        create }
    )

    return postcreate
  }

  /**
   * @private
   *
   * @param {*} args - The arguments a Resolver is called with.
   * @param {*} context - GraphQL context.
   * @param {*} info - GraphQL info.
   * @returns {*} The data returned by a Resolver's [postupdate]{@link Resolver#postupdate} phase.
   */
  async _update(args, context, info) {
    await this.authorize('update', context)

    const requestedAs = getRequestedAs(this.typeName, info)
    const requestedFields = getRequestedFields(this.fields, info)
    const requestedSubresolvers = getRequestedSubresolvers(this.fields, info)

    const preupdate = await this.preupdate(
      args,
      context,
      info,

      { requestedAs,
        requestedFields,
        requestedSubresolvers }
    )

    const update = await this.update(
      args,
      context,
      info,

      { requestedAs,
        requestedFields,
        requestedSubresolvers,
        preupdate }
    )

    const postupdate = await this.postupdate(
      args,
      context,
      info,

      { requestedAs,
        requestedFields,
        requestedSubresolvers,
        preupdate,
        update }
    )

    return postupdate
  }

  /**
   * @private
   *
   * @param {*} args - The arguments a Resolver is called with.
   * @param {*} context - GraphQL context.
   * @param {*} info - GraphQL info.
   * @returns {*} The data returned by a Resolver's [postdelete]{@link Resolver#postdelete} phase.
   */
  async _delete(args, context, info) {
    await this.authorize('delete', context)

    const requestedAs = getRequestedAs(this.typeName, info)
    const requestedFields = getRequestedFields(this.fields, info)
    const requestedSubresolvers = getRequestedSubresolvers(this.fields, info)

    const predelete = await this.predelete(
      args,
      context,
      info,

      { requestedAs,
        requestedFields,
        requestedSubresolvers }
    )

    const del = await this.delete(
      args,
      context,
      info,

      { requestedAs,
        requestedFields,
        requestedSubresolvers,
        predelete }
    )

    const postdelete = await this.postdelete(
      args,
      context,
      info,

      { requestedAs,
        requestedFields,
        requestedSubresolvers,
        predelete,
        delete: del }
    )

    return postdelete
  }

  /**
   * Called before a Resolver's [prefetch]{@link Resolver#prefetch},
   * [precreate]{@link Resolver#precreate},
   * [preupdate]{@link Resolver#preupdate},
   * or [predelete]{@link Resolver#predelete} phase,
   * and can be overriden to implement custom logic.
   *
   * @param {*} operation - GraphQL operation.
   * @param {*} context - GraphQL context.
   * @throws {Error}
   */
  async authorize(operation, context) {}

  /**
   * Called before a Resolver's [fetch]{@link Resolver#fetch} phase,
   * and can be overriden to implement custom logic.
   * Data returned here is passed on to the fetch phase.
   *
   * @param {*} parent - The data returned by a Resolver's parent.
   * @param {*} args - The arguments a Resolver is called with.
   * @param {*} context - GraphQL context.
   * @param {*} info - GraphQL info.
   * @param {ResolverContext} resolverContext - Resolver context.
   * @returns {*} null
   */
  async prefetch(parent, args, context, info, resolverContext) {
    return null
  }

  /**
   * Called before a Resolver's [postfetch]{@link Resolver#postfetch} phase,
   * and must be overriden.
   * Data returned here is passed on to the postfetch phase.
   *
   * @param {*} parent - The data returned by a Resolver's parent.
   * @param {*} args - The arguments a Resolver is called with.
   * @param {*} context - GraphQL context.
   * @param {*} info - GraphQL info.
   * @param {ResolverContext} resolverContext - Resolver context.
   * @throws {Error} If not overriden.
   * @returns {*}
   */
  async fetch(parent, args, context, info, resolverContext) {
    throw new Error(`${this.typeName}.fetch is not implemented.`)
  }

  /**
   * Called before a Resolver returns,
   * and can be overriden to implement custom logic.
   * Data returned here is returned by a Resolver.
   *
   * @param {*} parent - The data returned by a Resolver's parent.
   * @param {*} args - The arguments a Resolver is called with.
   * @param {*} context - GraphQL context.
   * @param {*} info - GraphQL info.
   * @param {ResolverContext} resolverContext - Resolver context.
   * @returns {*} The data returned by a Resolver's [fetch]{@link Resolver#fetch} phase.
   */
  async postfetch(parent, args, context, info, resolverContext) {
    return resolverContext.fetch
  }

  /**
   * Called before a Resolver's [create]{@link Resolver#create} phase,
   * and can be overriden to implement custom logic.
   * Data returned here is passed on to the create phase.
   *
   * @param {*} args - The arguments a Resolver is called with.
   * @param {*} context - GraphQL context.
   * @param {*} info - GraphQL info.
   * @param {ResolverContext} resolverContext - Resolver context.
   * @returns {*} null
   */
  async precreate(args, context, info, resolverContext) {
    return null
  }

  /**
   * Called before a Resolver's [postcreate]{@link Resolver#postcreate} phase,
   * and must be overriden.
   * Data returned here is passed on to the postcreate phase.
   *
   * @param {*} args - The arguments a Resolver is called with.
   * @param {*} context - GraphQL context.
   * @param {*} info - GraphQL info.
   * @param {ResolverContext} resolverContext - Resolver context.
   * @throws {Error} If not overriden.
   * @returns {*}
   */
  async create(args, context, info, resolverContext) {
    throw new Error(`${this.typeName}.create is not implemented.`)
  }

  /**
   * Called before a Resolver returns,
   * and can be overriden to implement custom logic.
   * Data returned here is returned by a Resolver.
   *
   * @param {*} args - The arguments a Resolver is called with.
   * @param {*} context - GraphQL context.
   * @param {*} info - GraphQL info.
   * @param {ResolverContext} resolverContext - Resolver context.
   * @returns {*} The data returned by a Resolver's [create]{@link Resolver#create} phase.
   */
  async postcreate(args, context, info, resolverContext) {
    return resolverContext.create
  }

  /**
   * Called before a Resolver's [update]{@link Resolver#update} phase,
   * and can be overriden to implement custom logic.
   * Data returned here is passed on to the update phase.
   *
   * @param {*} args - The arguments a Resolver is called with.
   * @param {*} context - GraphQL context.
   * @param {*} info - GraphQL info.
   * @param {ResolverContext} resolverContext - Resolver context.
   * @returns {*} null
   */
  async preupdate(args, context, info, resolverContext) {
    return null
  }

  /**
   * Called before a Resolver's [postupdate]{@link Resolver#postupdate} phase,
   * and must be overriden.
   * Data returned here is passed on to the postupdate phase.
   *
   * @param {*} args - The arguments a Resolver is called with.
   * @param {*} context - GraphQL context.
   * @param {*} info - GraphQL info.
   * @param {ResolverContext} resolverContext - Resolver context.
   * @throws {Error} If not overriden.
   * @returns {*}
   */
  async update(args, context, info, resolverContext) {
    throw new Error(`${this.typeName}.update is not implemented.`)
  }

  /**
   * Called before a Resolver returns,
   * and can be overriden to implement custom logic.
   * Data returned here is returned by a Resolver.
   *
   * @param {*} args - The arguments a Resolver is called with.
   * @param {*} context - GraphQL context.
   * @param {*} info - GraphQL info.
   * @param {ResolverContext} resolverContext - Resolver context.
   * @returns {*} The data returned by a Resolver's [update]{@link Resolver#update} phase.
   */
  async postupdate(args, context, info, resolverContext) {
    return resolverContext.update
  }

  /**
   * Called before a Resolver's [delete]{@link Resolver#delete} phase,
   * and can be overriden to implement custom logic.
   * Data returned here is passed on to the delete phase.
   *
   * @param {*} args - The arguments a Resolver is called with.
   * @param {*} context - GraphQL context.
   * @param {*} info - GraphQL info.
   * @param {ResolverContext} resolverContext - Resolver context.
   * @returns {*} null
   */
  async predelete(args, context, info, resolverContext) {
    return null
  }

  /**
   * Called before a Resolver's [postdelete]{@link Resolver#postdelete} phase,
   * and must be overriden.
   * Data returned here is passed on to the postdelete phase.
   *
   * @param {*} args - The arguments a Resolver is called with.
   * @param {*} context - GraphQL context.
   * @param {*} info - GraphQL info.
   * @param {ResolverContext} resolverContext - Resolver context.
   * @throws {Error} If not overriden.
   * @returns {*}
   */
  async delete(args, context, info, resolverContext) {
    throw new Error(`${this.typeName}.delete is not implemented.`)
  }

  /**
   * Called before a Resolver returns,
   * and can be overriden to implement custom logic.
   * Data returned here is returned by a Resolver.
   *
   * @param {*} args - The arguments a Resolver is called with.
   * @param {*} context - GraphQL context.
   * @param {*} info - GraphQL info.
   * @param {ResolverContext} resolverContext - Resolver context.
   * @returns {*} The data returned by a Resolver's [delete]{@link Resolver#delete} phase.
   */
  async postdelete(args, context, info, resolverContext) {
    return resolverContext.delete
  }
}

module.exports = Resolver
