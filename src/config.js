/** @file Load the configuration. */

/**
 * Object representing a configuration.
 * @typedef {Object} Configuration
 * @property {boolean} development - Whether the server is running in development mode.
 * @property {boolean} production - Whether the server is running in production mode.
 * @property {boolean} test - Whether the server is running in test mode.
 * @property {boolean} infoServer - Whether the Info server runs.
 * @property {string} infoPath - The Info server path.
 * @property {boolean} jsDocServer - Whether the JSDoc [server]{@link https://github.com/expressjs/serve-static} runs.
 * @property {Object} jsDocServerOptions - The JSDoc server options.
 * @property {string} jsDocPath - The JSDoc server path.
 * @property {string} graphQlPath - The GraphQL server path.
 * @property {number} port - The server port.
 */

/** @type {Configuration} */
const config = {
  development: process.env.LITE_SPEED_DEVELOPMENT == 'true',
  production: process.env.LITE_SPEED_PRODUCTION == 'true',
  test: process.env.LITE_SPEED_TEST == 'true',
  infoServer: process.env.LITE_SPEED_INFO_SERVER == 'true',
  infoPath: process.env.LITE_SPEED_INFO_PATH,
  jsDocServer: process.env.LITE_SPEED_JS_DOC_SERVER == 'true',
  jsDocServerOptions: JSON.parse(process.env.LITE_SPEED_JS_DOC_SERVER_OPTIONS),
  jsDocPath: process.env.LITE_SPEED_JS_DOC_PATH,
  graphQlPath: process.env.LITE_SPEED_GRAPH_QL_PATH,
  port: Number.parseInt(process.env.LITE_SPEED_PORT),
}

module.exports = config
