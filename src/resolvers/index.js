/** @file Load the Resolvers and convert them to GraphQL resolvers. */

const resolvers = require('./resolvers')
const toGraphQLResolver = require('../utils/to-graphql-resolver')

const graphQLResolvers = Object.values(resolvers)
  .map(resolver => toGraphQLResolver(resolver))
  .reduce(
    (graphQLResolvers, graphQLResolver) => {
      Object.assign(
        graphQLResolvers.Query,
        graphQLResolver.Query
      )

      Object.assign(
        graphQLResolvers.Mutation,
        graphQLResolver.Mutation
      )

      Object.assign(
        graphQLResolvers,
        { [graphQLResolver._typeName]: graphQLResolver[graphQLResolver._typeName] }
      )

      return graphQLResolvers
    },

    {
      Query: {},
      Mutation: {}
    }
  )

module.exports = graphQLResolvers
