/** @file Load the Resolvers. */

const schema = require('../schema/schema')
const Star = require('./star')
const Planet = require('./planet')
const Satellite = require('./satellite')

const star = new Star(schema)
const planet = new Planet(schema)
const satellite = new Satellite(schema)

const resolvers = {
  Star: star,
  Planet: planet,
  Satellite: satellite
}

module.exports = resolvers
