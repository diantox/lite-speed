const Resolver = require('../classes/resolver')
const postgres = require('../utils/postgres')
const getColumns = require('../utils/get-columns')

class Satellite extends Resolver {

  static table = 'satellite'
  static columns = {
    id: 'id',
    planetId: 'planet_id',
    name: 'name'
  }

  static parentHandlers = {
    'Planet': async (parent, args, context, info, { prefetch: { query } }) =>
      query.where(this.columns.planetId, parent.id)
  }

  static childrenHandlers = {
    'planet': async (parent, args, context, info, { prefetch: { columns } }) =>
      columns.planetId = this.columns.planetId
  }

  static argumentHandlers = {
    'ids': async (parent, { ids }, context, info, { prefetch: { query } }) =>
      query.whereIn(this.columns.id, ids),

    'planetIds': async (parent, { planetIds }, context, info, { prefetch: { query } }) =>
      query.whereIn(this.columns.planetId, planetIds)
  }

  async prefetch(parent, args, context, info, resolverContext) {
    const table = this.constructor.table
    const columns = getColumns(this.constructor.columns, resolverContext.requestedFields)
    const query = postgres(table)

    return {
      columns,
      query
    }
  }

  async fetch(parent, args, context, info, { prefetch: { columns, query } }) {
    return await query.select(columns)
  }

  async postfetch(parent, args, context, info, resolverContext) {
    if (resolverContext.requestedAs.list) {
      return resolverContext.fetch
    } else {
      return resolverContext.fetch[0]
    }
  }
}

module.exports = Satellite
