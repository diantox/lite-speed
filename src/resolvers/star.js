const Resolver = require('../classes/resolver')
const postgres = require('../utils/postgres')
const getColumns = require('../utils/get-columns')

class Star extends Resolver {

  static table = 'star'
  static columns = {
    id: 'id',
    name: 'name'
  }

  static parentHandlers = {
    'Planet': async (parent, args, context, info, { prefetch: { query } }) =>
      query.where(this.columns.id, parent.starId)
  }

  static childrenHandlers = {
    'planets': async (parent, args, context, info, { prefetch: { columns } }) =>
      columns.id = this.columns.id
  }

  static argumentHandlers = {    
    'ids': async (parent, { ids }, context, info, { prefetch: { query } }) =>
      query.whereIn(this.columns.id, ids)
  }

  async prefetch(parent, args, context, info, resolverContext) {
    const table = this.constructor.table
    const columns = getColumns(this.constructor.columns, resolverContext.requestedFields)
    const query = postgres(table)

    return {
      columns,
      query
    }
  }

  async fetch(parent, args, context, info, { prefetch: { columns, query } }) {
    return await query.select(columns)
  }

  async postfetch(parent, args, context, info, resolverContext) {
    if (resolverContext.requestedAs.list) {
      return resolverContext.fetch
    } else {
      return resolverContext.fetch[0]
    }
  }
}

module.exports = Star
