#!/usr/bin/env node

/** @file Load the GraphQL server. */

const path = require('path')
const express = require('express')
const { ApolloServer } = require('apollo-server-express')

const packageJSON = require('../package')
const config = require('./config')
const schema = require('./schema')

async function main() {
  const expressServer = express()
  const apolloServer = new ApolloServer({ schema })

  if (config.infoServer) {
    expressServer.get(
      config.infoPath,
      (req, res) => res.json({
        name: packageJSON.name,
        version: packageJSON.version
      })
    )
  } 

  if (config.jsDocServer) {
    expressServer.use(
      config.jsDocPath,
      express.static(
        path.join(__dirname, '../jsdoc'),
        config.jsDocServerOptions
      )
    )
  }

  apolloServer.applyMiddleware({
    app: expressServer,
    path: config.graphQlPath
  })

  expressServer.listen({ port: config.port })
}

main()
