/**
 * Get a list of fields a [Resolver]{@link Resolver} was called with.
 * @param {ResolverField[]} fields - A Resolver's fields.
 * @param {*} info - GraphQL info.
 * @returns {ResolverField[]} A list of fields a Resolver was called with.
 */
function getRequestedFields(fields, info) {
  const requestedFields = info.fieldNodes[0].selectionSet.selections
    .filter(field => !field.selectionSet)
    .map(field => fields[field.name.value])

  return requestedFields
}

module.exports = getRequestedFields
