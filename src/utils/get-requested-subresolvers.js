/**
 * Get a list of subresolvers a [Resolver]{@link Resolver} was called with.
 * @param {ResolverField[]} fields - A Resolver's fields.
 * @param {*} info - GraphQL info.
 * @returns {ResolverField[]} A list of subresolvers a Resolver was called with.
 */
function getRequestedSubresolvers(fields, info) {
  const requestedSubresolvers = info.fieldNodes[0].selectionSet.selections
    .filter(field => field.selectionSet)
    .map(field => fields[field.name.value])

  return requestedSubresolvers
}

module.exports = getRequestedSubresolvers
