/** @file Load Postgres. */

const knex = require('knex')
const config = require('../../knex/config')

const postgres = knex(config)

module.exports = postgres
