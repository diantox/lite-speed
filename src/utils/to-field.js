/**
 * Convert a GraphQL AST node to a [ResolverField]{@link ResolverField}.
 * @param {*} astNode - GraphQL AST node.
 * @return {ResolverField} A ResolverField.
 */
function toField(astNode) {
  const field = {}

  switch (astNode.kind) {
    case 'FieldDefinition':
      field.name = astNode.name.value
      Object.assign(
        field,
        toField(astNode.type)
      )
      break
    case 'NamedType':
      field.type = astNode.name.value
      field.primitive =
        field.type === 'Boolean' ||
        field.type === 'Int' ||
        field.type === 'Float' ||
        field.type === 'String' ||
        field.type === 'ID'
      break
    case 'ListType':
      field.list = true
      Object.assign(
        field,
        toField(astNode.type)
      )
      break
    case 'NonNullType':
      field.nonNull = true
      Object.assign(
        field,
        toField(astNode.type)
      )
      break
    default:
      throw new Error(`Unknown AST node kind: ${astNode.kind}`)
  }

  return field
}

module.exports = toField
