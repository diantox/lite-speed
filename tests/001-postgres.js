const chai = require('chai')
const postgres = require('../src/utils/postgres')

describe('Postgres should work', () => {
  it('Postgres host should be 172.17.0.1', () => 
    chai.expect(postgres.client.config.connection.host).to.equal('172.17.0.1'))

  it('Postgres should rollback all migrations.', () =>
    postgres.migrate.rollback(true))

  it('Postgres should run all migrations.', () =>
    postgres.migrate.latest())

  it('Postgres should run all seeds.', () =>
    postgres.seed.run())
})
