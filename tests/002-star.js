const chai = require('chai')
const chaiHTTP = require('chai-http')

const config = require('../src/config')

chai.use(chaiHTTP)

const starsQuery = `
  query($ids: [ID]) {
    star(ids: $ids) {
      id
      name
    }
  }
`

const planetsWithStarsQuery = `
  query($ids: [ID]) {
    planet(ids: $ids) {
      id
      starId
      name

      star {
        id
        name
      }
    }
  }
`

describe('Star resolver should work.', () => {
  it('Star resolver should return all stars.', () => 
    chai
      .request(`http://localhost:${config.port}`)
      .post(config.graphQlPath)
      .send({ query: starsQuery })
      .then(res => {
        chai.expect(res).to.have.status(200)
        chai.expect(res).to.be.json
        chai.expect(res.body.data.star).to.have.deep.ordered.members([
          {
            id: "0",
            name: "Sol"
          },
          { 
            id: "1",
            name: "Proxima Centauri"
          }
        ])
      }))

  it('Star resolver should return Sol', () => 
    chai
      .request(`http://localhost:${config.port}`)
      .post(config.graphQlPath)
      .send({
        query: starsQuery,
        variables: { ids: [0] }
      })
      .then(res => {
        chai.expect(res).to.have.status(200)
        chai.expect(res).to.be.json
        chai.expect(res.body.data.star).to.have.deep.ordered.members([
          {
            id: "0",
            name: "Sol"
          }
        ])
      }))

  it('Planet resolver should return all planets with stars.', () => 
    chai
      .request(`http://localhost:${config.port}`)
      .post(config.graphQlPath)
      .send({ query: planetsWithStarsQuery })
      .then(res => {
        chai.expect(res).to.have.status(200)
        chai.expect(res).to.be.json
        chai.expect(res.body.data.planet).to.have.deep.ordered.members([
          {
            "id": "0",
            "name": "Mercury",
            "starId": "0",
            "star": {
              "id": "0",
              "name": "Sol"
            }
          },
          {
            "id": "1",
            "name": "Venus",
            "starId": "0",
            "star": {
              "id": "0",
              "name": "Sol"
            }
          },
          {
            "id": "2",
            "name": "Earth",
            "starId": "0",
            "star": {
              "id": "0",
              "name": "Sol"
            }
          },
          {
            "id": "3",
            "name": "Mars",
            "starId": "0",
            "star": {
              "id": "0",
              "name": "Sol"
            }
          },
          {
            "id": "4",
            "name": "Jupiter",
            "starId": "0",
            "star": {
              "id": "0",
              "name": "Sol"
            }
          },
          {
            "id": "5",
            "name": "Saturn",
            "starId": "0",
            "star": {
              "id": "0",
              "name": "Sol"
            }
          },
          {
            "id": "6",
            "name": "Uranus",
            "starId": "0",
            "star": {
              "id": "0",
              "name": "Sol"
            }
          },
          {
            "id": "7",
            "name": "Neptune",
            "starId": "0",
            "star": {
              "id": "0",
              "name": "Sol"
            }
          },
          {
            "id": "8",
            "name": "Proxima Centauri B",
            "starId": "1",
            "star": {
              "id": "1",
              "name": "Proxima Centauri"
            }
          },
          {
            "id": "9",
            "name": "Proxima Centauri C",
            "starId": "1",
            "star": {
              "id": "1",
              "name": "Proxima Centauri"
            }
          }
        ])
      }))
})
