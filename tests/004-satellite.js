const chai = require('chai')
const chaiHTTP = require('chai-http')

const config = require('../src/config')
const { Planets } = require('../src/resolvers/planet')
const { Satellites } = require('../src/resolvers/satellite')

chai.use(chaiHTTP)

const satellitesQuery = `
  query(
    $ids: [ID],
    $planetIds: [ID],
  ) {
    satellite(
      ids: $ids,
      planetIds: $planetIds
    ) {
      planetId
      id
      name
    }
  }
`

const planetsWithSatellitesQuery = `
  query($ids: [ID]) {
    planet(ids: $ids) {
      starId
      id
      name

      satellites {
        planetId
        id
        name
      }
    }
  }
`

describe('Satellite resolver should work.', () => {
  it('Satellite resolver should return all satellites.', () => 
    chai
      .request(`http://localhost:${config.port}`)
      .post(config.graphQlPath)
      .send({ query: satellitesQuery })
      .then(res => {
        chai.expect(res).to.have.status(200)
        chai.expect(res).to.be.json
        chai.expect(res.body.data.satellite).to.have.deep.ordered.members([
          {
            "id": "0",
            "planetId": "2",
            "name": "Moon"
          },
          {
            "id": "1",
            "planetId": "3",
            "name": "Phobos"
          },
          {
            "id": "2",
            "planetId": "3",
            "name": "Deimos"
          }
        ])
      }))

  it('Satellite resolver should return Moon', () => 
    chai
      .request(`http://localhost:${config.port}`)
      .post(config.graphQlPath)
      .send({
        query: satellitesQuery,
        variables: { ids: [0] }
      })
      .then(res => {
        chai.expect(res).to.have.status(200)
        chai.expect(res).to.be.json
        chai.expect(res.body.data.satellite).to.have.deep.ordered.members([
          {
            "id": "0",
            "planetId": "2",
            "name": "Moon"
          }
        ])
      }))

  it("Satellite resolver should return Earth's satellites.", () => 
    chai
      .request(`http://localhost:${config.port}`)
      .post(config.graphQlPath)
      .send({
        query: satellitesQuery,
        variables: { planetIds: [2] }
      })
      .then(res => {
        chai.expect(res).to.have.status(200)
        chai.expect(res).to.be.json
        chai.expect(res.body.data.satellite).to.have.deep.ordered.members([
          {
            "id": "0",
            "planetId": "2",
            "name": "Moon"
          }
        ])
      }))

  it('Planet resolver should return all planets with satellites.', () => 
    chai
      .request(`http://localhost:${config.port}`)
      .post(config.graphQlPath)
      .send({ query: planetsWithSatellitesQuery })
      .then(res => {
        chai.expect(res).to.have.status(200)
        chai.expect(res).to.be.json
        chai.expect(res.body.data.planet).to.have.deep.ordered.members([
          {
            "id": "0",
            "starId": "0",
            "name": "Mercury",
            "satellites": []
          },
          {
            "id": "1",
            "starId": "0",
            "name": "Venus",
            "satellites": []
          },
          {
            "id": "2",
            "starId": "0",
            "name": "Earth",
            "satellites": [
              {
                "id": "0",
                "planetId": "2",
                "name": "Moon"
              }
            ]
          },
          {
            "id": "3",
            "starId": "0",
            "name": "Mars",
            "satellites": [
              {
                "id": "1",
                "planetId": "3",
                "name": "Phobos"
              },
              {
                "id": "2",
                "planetId": "3",
                "name": "Deimos"
              }
            ]
          },
          {
            "id": "4",
            "starId": "0",
            "name": "Jupiter",
            "satellites": []
          },
          {
            "id": "5",
            "starId": "0",
            "name": "Saturn",
            "satellites": []
          },
          {
            "id": "6",
            "starId": "0",
            "name": "Uranus",
            "satellites": []
          },
          {
            "id": "7",
            "starId": "0",
            "name": "Neptune",
            "satellites": []
          },
          {
            "id": "8",
            "starId": "1",
            "name": "Proxima Centauri B",
            "satellites": []
          },
          {
            "id": "9",
            "starId": "1",
            "name": "Proxima Centauri C",
            "satellites": []
          }
        ])
      }))
})
